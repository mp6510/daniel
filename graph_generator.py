# graph_generator.py - generate a complete graph of nodes with a random connection
# Author: Michael Peechatt

import argparse
import random

# Get argumenets from command line
parser = argparse.ArgumentParser()
parser.add_argument("number_of_nodes", type=int,
                    help="number of nodes you want to create")
parser.add_argument("connection_rate", type=float,
                    help="value between [0.0, 1.0]")
parser.add_argument("-s", "--seed", type=int,
                    help="seed value used to reproduce results")
args = parser.parse_args()

# Set random seed
if args.seed:
    random.seed(args.seed)

# Write out the daniel file 
f = open(f"{args.number_of_nodes}_node_graph.daniel", "w")

# Write connections from start node
for i in range(args.number_of_nodes):
    if random.random() <= args.connection_rate:
        output_string = ""
        output_string += f"(Start) Test Case for Node {i}\n1. {{{i}}}\n\n"
        f.write(output_string)

# Write complete connections between nodes
for i in range(args.number_of_nodes):
    for j in range(args.number_of_nodes):
        if random.random() <= args.connection_rate:
            output_string = ""
            output_string += f"(Start) Test Case for Node {i}\n1. {{{i}}}\n2. {{{j}}}\n\n"
            f.write(output_string)