# selenium2daniel.py - convert selenium script to DANIEL graph
# Author: Michael Peechatt

import re
import warnings
import argparse

# Get filename from command line
parser = argparse.ArgumentParser()
parser.add_argument("selenium_test", type=str,
                    help="selenium python file with steps for test case")
args = parser.parse_args()

# Read in user file
sample = open(args.selenium_test, "r")
s = sample.read()

# Ouptut string that will output DANIEL test case
output_string = ""

# Get test case title
title = re.search(r'# Title\:.*?\n', s)
if title:
    title = title.group(0).split(':')[1]
    title = re.sub(r'(^\s|\n$)', '', title)
else:
    raise Exception("No test case title found in Selenium script")

if not re.search(r'# State\:', s):
    raise Exception("No states defined in Selenium script")

# Remove the dropdown option
s = re.sub(r'dropdown \= ', '', s)
s = re.sub(r'\n.*?dropdown\.', '.', s)

# Separate steps by state
state_code_chunk = re.split(r'# State\:', s)
# Remove the chunk of code before the states
state_code_chunk.pop(0)

# Add test case title to output string
output_string += f"(Start) {title}\n"

# Keep a running count of steps for output
step_count = 1

# Cycle through all steps in the chunks of code by state
for lines in state_code_chunk:
    steps = re.split(r'\n', lines)
    # Get the current states
    state = steps[0].strip()
    # Loop through all the steps in that state
    for step in steps[1:]:
        # Search for selenium actions
        if "XPATH" in step:
            # Special exception for XPATHs which can include parenthesis
            actions = re.findall(r'(\(.*?\)\.|\(\))', step)
            actions[0] = actions[0][:-1]
            # Remove extra period if input also is an XPATH
            if "XPATH" in actions[1]:
                actions[1] = actions[1][:-1]
        else:
            # Grab selenium actions
            actions = re.findall(r'\(.*?\)', step)
        if len(actions) > 0:
            step_string = ""
            # Get the first action and remove surrounding parenthesis
            action = actions[0][1:-1]
            # Begin writing test case step
            step_string += f"{step_count}. {{<{state}> {action}}}"
            # Increment step count
            step_count += 1
            # Graph the input value
            input_val = actions[1][1:-1]
            # If there is an input value, add it to the end
            if len(input_val):
                step_string += f" [{input_val}]"
            step_string += "\n"
            output_string += step_string

# Print out converted selenium test case to stdout
print(output_string)