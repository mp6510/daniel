# DANIEL: Directed Action Node Input Execution Language

Create a directed graph from test cases. Then generate new test cases
by traversing the graph with a random walk, a weighted walk (based on
which paths are fixed bugs, new features, or known issues), or a walk guided by machine learning models.

{ } = action  
< > = state  
[ ] = input  

## Read the Thesis

**PDF:** [Download here](https://gitlab.com/mp6510/daniel/-/raw/master/MS_DANIEL_Corrections.pdf?inline=false)

<!-- ## Watch the Thesis Defense

**Video:** [https://vimeo.com/494525221](https://vimeo.com/494525221)  
**Password:** danielpres -->

## Requirements

* python3
* graphviz
* numpy
* xgboost
* sklearn
* gensim

```
$ pip3 install graphviz
$ pip3 install numpy
$ pip3 install xgboost
$ pip3 install sklearn
$ pip3 install gensim
```

## Usage

```
$ python3 daniel.py --help
usage: daniel.py [-h] [-fb FIXED_BUGS] [-nf NEW_FEATURES] [-ki KNOWN_ISSUES] [-rw RANDOM_WALK] [-mw MODEL_WALK]
                 [-xgbm XGBOOST_MODEL_WALK] [-em] [-ae] [-el] [-gs] [-ew]
                 test_cases max_steps num_tests

positional arguments:
  test_cases            daniel file with input test cases to build graph
  max_steps             specify number of max steps for each generated test case
  num_tests             specify number of test cases to be generated

optional arguments:
  -h, --help            show this help message and exit
  -fb FIXED_BUGS, --fixed_bugs FIXED_BUGS
                        daniel file with test cases of fixed bugs; edge weights * 2.0
  -nf NEW_FEATURES, --new_features NEW_FEATURES
                        daniel file with test cases of new features; edge weights * 1.5
  -ki KNOWN_ISSUES, --known_issues KNOWN_ISSUES
                        daniel file with test cases of known issues; edge weights * 0.5
  -rw RANDOM_WALK, --random_walk RANDOM_WALK
                        specify number of test cases to be generated in random walk output
  -mw MODEL_WALK, --model_walk MODEL_WALK
                        specify number of test cases to be generated in model walk output
  -xgbm XGBOOST_MODEL_WALK, --xgboost_model_walk XGBOOST_MODEL_WALK
                        specify number of test cases to be generated in xgboost model walk output
  -em, --evaluate_model
                        boolean flag to evaluate model performance with test train split
  -ae, --average_embeddings
                        boolean flag to average vector embeddings together
  -el, --edge_labels    boolean flag to draw input edge labels on graph
  -gs, --graph_structure
                        boolean flag to print out graph object structure
  -ew, --ending_weights
                        boolean flag to print out ending weights of each node
```

## Example Input

```
$ vim example_input.daniel

(Start) First Test Case Example Label: 0
1. {<State 1> Action 1} [Input 1]
2. {<State 1> Action 2}
3. {<State 1> Action 3}
4. {<State 1> Action 1}

(Start) Second Test Case Example Label: 1
1. {<State 1> Action 4}
2. {<State 2> Action 5} [Input 3]
3. {<State 1> Action 1} [Input 2]
4. {<State 1> Action 4}
```

## Example Output

```
$ python3 daniel.py example_input.daniel 5 5 -el -gs -ew
```

![output DANIEL graph](https://i.imgur.com/GLDJCB1.png)

```
=== Graph Object Structure ===
{
    '<State 1> Action 1': {
        '<State 1> Action 2': {
            'edge_weight': 1,
            'inputs_and_weights': {
                'lambda': 1
            }
        },
        '<State 1> Action 4': {
            'edge_weight': 1,
            'inputs_and_weights': {
                'lambda': 1
            }
        }
    },
    '<State 1> Action 2': {
        '<State 1> Action 3': {
            'edge_weight': 1,
            'inputs_and_weights': {
                'lambda': 1
            }
        }
    },
    '<State 1> Action 3': {
        '<State 1> Action 1': {
            'edge_weight': 1,
            'inputs_and_weights': {
                'lambda': 1
            }
        }
    },
    '<State 1> Action 4': {
        '<State 2> Action 5': {
            'edge_weight': 1,
            'inputs_and_weights': {
                'Input 3': 1
            }
        }
    },
    '<State 2> Action 5': {
        '<State 1> Action 1': {
            'edge_weight': 1,
            'inputs_and_weights': {
                'Input 2': 1
            }
        }
    },
    'Start': {
        '<State 1> Action 1': {
            'edge_weight': 1,
            'inputs_and_weights': {
                'Input 1': 1
            }
        },
        '<State 1> Action 4': {
            'edge_weight': 1,
            'inputs_and_weights': {
                'lambda': 1
            }
        }
    }
}

=== Ending Weights ===
{'<State 1> Action 1': 1,
 '<State 1> Action 2': 0,
 '<State 1> Action 3': 0,
 '<State 1> Action 4': 1,
 '<State 2> Action 5': 0,
 'Start': 0}
```
