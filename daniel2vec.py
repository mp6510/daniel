# daniel2vec.py - test case embeddings using word2vec
# Author: Michael Peechatt

import re
import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from nltk.tokenize import sent_tokenize, word_tokenize
from gensim.models import Word2Vec
import xgboost as xgb

# Constant for defining the number of iterations for training the model
MAX_ITER = 1000

def get_test_case_vec(model, test_case, size=10, average=False):
    """
    Get vectorized representation of test case

    :param model: trained bag of words model for generating vector
    :param test_case: input test case string
    :param size: the number of dimensions of the output vector
    :param average: boolean to average word vectors
    :return: a vector representation of input test case
    """
    if model == None:
        raise Exception("No model specified")

    # Repalce each new line with space
    test_case = test_case.replace("\n", " ")
    test_tokenized = []

    # Iterate through each sentence in test case
    for i in sent_tokenize(test_case):
        temp = []     
        # Tokenize the sentence into words 
        for j in word_tokenize(i):
            temp.append(j.lower()) 
        test_tokenized.append(temp) 
    
    # Store num tokens for average operation
    num_tokens = 0
    # Iterate through each word, adding each 
    test_case_vec = np.zeros(size)
    for line in test_tokenized:
        for word in line:
            test_case_vec = np.add(test_case_vec, model.wv[word])
            num_tokens += 1
    
    if average:
        test_case_vec = test_case_vec / num_tokens

    return test_case_vec

def initialize_model(corpus_file, size=10, model_type="Word2Vec"):
    """
    Initialize Word2Vec bag of words model

    :param corpus_file: file of all test cases to build up model vocabulary
    :param size: number of dimensions for model
    :param model_type: a string for the type of trained model
    :return: trained word2vec model
    """
    # Read in test case corpus
    sample = open(corpus_file, "r") 
    s = sample.read() 

    # Remove all titles from the test cases
    s = re.sub(r'\(Start\).*?\n', '', s)

    # Repalce new line with space
    f = s.replace("\n", " ") 

    # List used for to hold tokenized corpus
    corpus = [] 
    
    # iterate through each sentence in the file 
    for i in sent_tokenize(f):
        temp = []     
        # tokenize the sentence into words 
        for j in word_tokenize(i): 
            temp.append(j.lower()) 
        corpus.append(temp)
    
    if model_type == "Word2Vec":
        # Create CBOW model 
        model = Word2Vec(corpus, min_count=1, size=size, window=5)
    
    return model

def load_test_cases(test_case_corpus, average=False):
    """
    Create X and y dataset

    :param test_case_corpus: file with all test cases to build up model vocabulary
    :return: a tuple of the vector model, X, and y
    """
    # Initialize the model using all the test case inputs
    vec_model = initialize_model(corpus_file=test_case_corpus, model_type="Word2Vec")
    
    # Read in all test cases from file
    all_test_cases = open(test_case_corpus, "r")
    t = all_test_cases.read()

    # Get labels from file
    y = []
    titles = re.findall(r'\(Start\).*?\n', t)
    for title in titles:
        # Grab the label, the last character in the title
        y.append(int(title[-2]))

    # Split the test cases by their title
    test_cases = re.split(r'\(Start\).*?\n', t)
    test_cases.pop(0)

    X = []
    # Get vector representation of each test case and store in data
    for test_case in test_cases:
        X.append(get_test_case_vec(vec_model, test_case, average=average))

    return (vec_model, np.array(X), np.array(y))

def write_out_csv(X, y, output_filename="embedded_test_cases.csv"):
    """
    Write out csv of embedded test cases with labels

    :param X: array of embedded test cases
    :param y: array of corresponding labels
    :param output_filename: output output_filename
    :return: a tuple of the vector model, X, and y
    """
    # Create dataframe with data and labels
    vectorized_tests_df = pd.DataFrame(np.hstack((X,np.transpose(np.array([y])))))

    # Write out CSV file
    vectorized_tests_df.to_csv(output_filename, header=False, index=False)

def get_logistic_reg_model(X, y, evaluate=False):
    """
    Get regression model trained on X and y dataset

    :param X: array of embedded test cases
    :param y: array of corresponding labels
    :param evaluate: boolean to print out model evaluation
    :return: regression model trained on embedded test cases
    """
    if evaluate:
        # Create test train split, 80-20
        arr_rand = np.random.rand(X.shape[0])
        split = arr_rand < np.percentile(arr_rand, 80)

        X_train = X[split]
        y_train = y[split]
        X_test =  X[~split]
        y_test = y[~split]

        # Fit model
        clf = LogisticRegression(random_state=0, max_iter=MAX_ITER).fit(X_train, y_train)

        # Print out predictions
        print("\n=== Model Performance ===")
        print(f"model score: {clf.score(X_test, y_test)}")
        predictions = clf.predict(X_test)
        print(f"predicted labels: {predictions}")
        print(f"actual labels: {y_test}")

        # Confusion matrix output
        print("\n=== Confusion Matrix ===")
        print(confusion_matrix(y_test, predictions))

        # Precision, Recall, and F1 scores
        print("\n=== Precision, Recall, and F1 ===")
        print(f"precision: {precision_score(y_test, predictions)}")
        print(f"recall: {recall_score(y_test, predictions)}")
        print(f"f1: {f1_score(y_test, predictions)}")

        return clf
    else:
        # Fit model and return
        return LogisticRegression(random_state=0, max_iter=MAX_ITER).fit(X, y)

def evaluate_validation_data(X_valid, y_valid, reg_model):
    """
    Evaluate logistic regression model performance on validation set

    :param X_valid: array of embedded validation test cases
    :param y_valid: array of corresponding validation labels
    :param reg_model: regression model
    """
    # Print out predictions
    print("\n=== Validation Performance ===")
    print(f"validation score: {reg_model.score(X_valid, y_valid)}")
    predictions = reg_model.predict(X_valid)
    print(f"predicted labels: {predictions}")
    print(f"actual labels: {y_valid}")

    # Confusion matrix output
    print("\n=== Confusion Matrix ===")
    print(confusion_matrix(y_valid, predictions))

    # Precision, Recall, and F1 scores
    print("\n=== Precision, Recall, and F1 ===")
    print(f"precision: {precision_score(y_valid, predictions)}")
    print(f"recall: {recall_score(y_valid, predictions)}")
    print(f"f1: {f1_score(y_valid, predictions)}")

def evaluate_xgb_validation_data(X_valid, y_valid, xgb_model):
    """
    Evaluate xgboost model performance on validation set

    :param X_valid: array of embedded validation test cases
    :param y_valid: array of corresponding validation labels
    :param xgb_model: xgboost model used to evaluate
    """
    dtest = xgb.DMatrix(np.array(X_valid), label=np.array(y_valid))
    predictions = []
    for prediction in xgb_model.predict(dtest):
        if prediction[1] >= 0.5:
            predictions.append(1)
        else:
            predictions.append(0)

    # Calculate xgboost model score
    correct_guesses = 0
    for i, prediction in enumerate(predictions):
        if prediction == y_valid[i]:
            correct_guesses += 1
    model_score = correct_guesses / len(y_valid)

    # Print out predictions
    print("\n=== XGBoost Validation Performance ===")
    print(f"model score: {model_score}")
    print(f"predicted labels: {predictions}")
    print(f"actual labels: {y_valid}")

    # Confusion matrix output
    print("\n=== Confusion Matrix ===")
    print(confusion_matrix(y_valid, predictions))

    # Precision, Recall, and F1 scores
    print("\n=== Precision, Recall, and F1 ===")
    print(f"precision: {precision_score(y_valid, predictions)}")
    print(f"recall: {recall_score(y_valid, predictions)}")
    print(f"f1: {f1_score(y_valid, predictions)}")

def get_XGBoost_model(X, y, evaluate=False):
    """
    Get XGBoost model trained on X and y dataset

    :param X: array of embedded test cases
    :param y: array of corresponding labels
    :param evaluate: boolean to print out model evaluation
    :return: XGBoost model trained on embedded test cases
    """

    # XGBoost model paramaters
    param = {
        'max_depth': 10,  # the maximum depth of each tree
        'eta': 0.3,  # the training step for each iteration
        'objective': 'multi:softprob',  # error evaluation for multiclass training
        'num_class': 2}  # the number of classes that exist in this datset
        
    # the number of training iterations
    num_round = 20

    if evaluate:
        # Create test train split, 80-20
        arr_rand = np.random.rand(X.shape[0])
        split = arr_rand < np.percentile(arr_rand, 80)

        X_train = X[split]
        y_train = y[split]
        X_test =  X[~split]
        y_test = y[~split]

        # Fit model
        dtrain = xgb.DMatrix(X_train, label=y_train)
        dtest = xgb.DMatrix(X_test, label=y_test)

        bst_model = xgb.train(param, dtrain, num_round)
        predictions = []
        for prediction in bst_model.predict(dtest):
            if prediction[1] >= 0.5:
                predictions.append(1)
            else:
                predictions.append(0)
        
        # Calculate xgboost model score
        correct_guesses = 0
        for i, prediction in enumerate(predictions):
            if prediction == y_test[i]:
                correct_guesses += 1
        model_score = correct_guesses / len(y_test)

        # Print out predictions
        print("\n=== XGBoost Model Performance ===")
        print(f"model score: {model_score}")
        print(f"predicted labels: {predictions}")
        print(f"actual labels: {y_test}")

        # Confusion matrix output
        print("\n=== Confusion Matrix ===")
        print(confusion_matrix(y_test, predictions))

        # Precision, Recall, and F1 scores
        print("\n=== Precision, Recall, and F1 ===")
        print(f"precision: {precision_score(y_test, predictions)}")
        print(f"recall: {recall_score(y_test, predictions)}")
        print(f"f1: {f1_score(y_test, predictions)}")

        return bst_model
    else:
        return xgb.train(param, xgb.DMatrix(X, label=y), num_round)