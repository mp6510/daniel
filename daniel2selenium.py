# daniel2selenium.py - convert a collection of DANIEL graphs to Selenium scripts
# Author: Michael Peechatt

import re
import os
import argparse

# Get filename from command line
parser = argparse.ArgumentParser()
parser.add_argument("daniel_test_cases", type=str,
                    help="file with collection of DANIEL test cases")
args = parser.parse_args()

# Read in user file
sample = open(args.daniel_test_cases, "r")
s = sample.read()

# Create directory that will store all the output scripts
os.makedirs("./generated_selenium_scripts")

# Grab test cases, removing the first empty one
test_cases = re.split(r'\(Start\).*?\n', s)
test_cases.pop(0)

# Keep a running test case count
test_case_count = 1

for test_case in test_cases:
    # Include the boilerplate imports
    output_script_string = '''from time import sleep
from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager

from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.service import Service

# Set the driver 
driver = webdriver.Firefox(service=Service(GeckoDriverManager().install()))

# Load the website
driver.get("https://mptest.5gconn.com/")

'''
    # Get each step in test case
    steps = re.split(r'\n', test_case)
    # Remove empty string steps
    while "" in steps:
        steps.remove("")
    for step in steps:
        # Parse selenium find element argument from step
        selenium_args = re.search(r'By\..*?(?=\})', step).group(0)
        output_script_string += f"driver.find_element({selenium_args})"
        # Parse input square brackets for other selenium actions
        selenium_args = re.search(r'(?<=\} \[).*(?=\])', step)
        if selenium_args:
            if "By.XPATH" in selenium_args.group(0):
                # Dropdown menu selection
                output_script_string += f".find_element({selenium_args.group(0)}).click()\n"
            else:
                # Type in a text field
                output_script_string += f".send_keys({selenium_args.group(0)})\n"
        else:
            # Otherwise just end with a click
            output_script_string += f".click()\n"
    # Write out the script file 
    f = open(f"./generated_selenium_scripts/{test_case_count}.py", "w")
    f.write(output_script_string)

    test_case_count += 1
