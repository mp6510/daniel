# daniel.py - intelligent test case graph traversal
# Author: Michael Peechatt

import re
import pprint
import random
import argparse
import numpy as np
import daniel2vec as d2v
import xgboost as xgb
from graphviz import Digraph

# Constant for defining min number of steps per test case
MIN_STEPS = 2

# Graph dictionary object
graph = {}

# Ending weights of each node, used for ending generated tests
ending_weights = {}

# Add graph components
def add_node(node):
    if node not in graph:
        graph[node] = {}
        ending_weights[node] = 0


def add_edge(source, destination):
    if not is_connected(source, destination):
        graph[source][destination] = {}
        graph[source][destination]['inputs_and_weights'] = {}
        graph[source][destination]['edge_weight'] = 1


def add_input(source, destination, input_to_add):
    if input_to_add not in get_inputs(source, destination):
        graph[source][destination]['inputs_and_weights'][input_to_add] = 1


# Remove graph components
def delete_node(node):
    if has_node(node):
        for n in get_nodes():
            if node in graph[n]:
                del graph[n][node]
        del ending_weights[node]
        del graph[node]


# Get graph attributes
def get_nodes():
    return list(graph.keys())


def get_neighbors(node):
    return list(graph[node].keys())


def get_total_neighbor_edge_weight(node):
    total_weight = 0
    for n in get_neighbors(node):
        total_weight += get_edge_weight(node, n)
    return total_weight


def get_neighbor_probability_distribution(node):
    total_weight = get_total_neighbor_edge_weight(node)
    probability_distribution = []
    for n in get_neighbors(node):
        probability_distribution.append(
            get_edge_weight(node, n) / total_weight)
    return probability_distribution


def get_edge_info(source, destination):
    return graph[source][destination]


def get_inputs(source, destination):
    return list(graph[source][destination]
                ['inputs_and_weights'].keys())


def get_input_weight(source, destination, input_to_find_weight):
    return graph[source][destination]['inputs_and_weights'][input_to_find_weight]


def get_input_weights(source, destination):
    return list(graph[source][destination]
                ['inputs_and_weights'].values())


def get_total_input_weight(source, destination):
    total_input_weight = 0
    for w in get_input_weights(source, destination):
        total_input_weight += w
    return total_input_weight


def get_input_probability_distribution(source, destination):
    total_input_weight = get_total_input_weight(source, destination)
    probability_distribution = []
    for i in get_inputs(source, destination):
        probability_distribution.append(
            get_input_weight(source, destination, i) / total_input_weight)
    return probability_distribution


def get_edge_weight(source, destination):
    return graph[source][destination]['edge_weight']


def get_ending_weight(node):
    return ending_weights[node]


def get_total_ending_weight():
    total_ending_weight = 0
    for n in get_nodes():
        total_ending_weight += get_ending_weight(n)
    return total_ending_weight


# Modify weights
def adjust_ending_weight(node, amount):
    ending_weights[node] += amount


def adjust_edge_weight(source, destination, amount):
    graph[source][destination]['edge_weight'] *= amount


def adjust_input_weight(source, destination, input_to_adjust, amount):
    graph[source][destination]['inputs_and_weights'][input_to_adjust] *= amount


# Check graph structure
def is_connected(source, destination):
    return destination in graph[source]


def has_node(node):
    return node in graph


# Build graph from file
def build_graph(fname, weight_adjust=1):
    """
    Build the underlying graph structure dictionary

    :param fname: input filename which has DANIEL test cases
    :param weight_adjust: how much each weight is multipled by
    :return: returns nothing
    """
    # Create start node
    add_node('Start')
    previous_node = 'Start'

    # Read in the file
    with open(fname) as f:
        lines = [line.rstrip('\n') for line in f]

        # Loop through each line of input file
        for l in lines:
            # Beginning of new test case
            if re.match(r'^\(Start\)', l) and previous_node != 'Start':
                # Increment ending weight of last node from previous test case
                adjust_ending_weight(previous_node, 1)

                # Reset previous node start for new test case
                previous_node = 'Start'

            # Library function call to add_node
            elif re.match(r'^add_nodes\((.*)\)', l):
                node_list = re.search(r'\((.*)\)', l)
                if node_list:
                    # Add node to graph
                    nodes = node_list.group(1).split(',')
                    nodes = [n.strip() for n in nodes]
                    for n in nodes:
                        add_node(n)

            # Library function call to add_edge
            elif re.match(r'^add_edge\((.*)\)', l):
                edge_list = re.search(r'\((.*)\)', l)

                if edge_list:
                    # Add edge between nodes
                    edge_args = edge_list.group(1).split(',')
                    edge_args = [e.strip() for e in edge_args]
                    add_edge(edge_args[0], edge_args[1])
                    adjust_edge_weight(
                        edge_args[0], edge_args[1], weight_adjust)

                    # Add inputs
                    for i in range(2, len(edge_args)):
                        add_input(edge_args[0], edge_args[1], edge_args[i])
                        adjust_input_weight(edge_args[0],
                                            edge_args[1], edge_args[i], weight_adjust)

                    # Add lambda input if no inputs provided
                    if len(edge_args) == 2:
                        add_input(edge_args[0], edge_args[1], 'lambda')

            # Library function call to delete_node
            elif re.match(r'^delete_node\((.*)\)', l):
                node_to_delete = re.search(r'\((.*)\)', l)
                delete_node(node_to_delete.group(1))

            # Next step node in test case
            else:
                # Find current node
                node = re.search(r'\{(.*)\}', l)
                if node:
                    # If node containts ':' character, throw exception
                    if ":" in node.group(1):
                        raise Exception("{" + node.group(1) + "}\nNodes are not allowed to use colon character ':'")

                    # Add node to graph
                    add_node(node.group(1))

                    # Add edge connection
                    add_edge(previous_node, node.group(1))

                    # Adjust edge weight bias
                    adjust_edge_weight(
                        previous_node, node.group(1), weight_adjust)

                    # Find input
                    input_to_add = re.search(r"\} \[(.*)\]", l)
                    if input_to_add:
                        # Add input between current and previous node
                        add_input(previous_node,
                                  node.group(1), input_to_add.group(1))

                        # Adjust input weight bias
                        adjust_input_weight(previous_node,
                                            node.group(1), input_to_add.group(1), weight_adjust)

                    else:
                        # If no input defined, use lambda
                        add_input(previous_node,
                                  node.group(1), 'lambda')

                    # Set previous node to current
                    previous_node = node.group(1)

    # Increment ending weight of the last test case
    adjust_ending_weight(previous_node, 1)


# Draw and output graph file
def draw_graph(show_inputs=True):
    """
    Write to file test cases generated by random walk

    :param show_inputs: a boolean value to include inputs in output PDF
    :return: returns nothing
    """
    # Create graph title
    f = Digraph('daniel_graph', filename='daniel.gv')

    # Define node shape attributes
    f.attr(rankdir='LR', size='8,5')
    f.attr('node', shape='circle')

    # Define all the nodes
    for node in get_nodes():
        if node == 'Start':
            f.attr('node', shape='doublecircle')
            f.node(node)
            f.attr('node', shape='circle')
        else:
            f.node(node)

    # Draw edges and inputs
    for source in get_nodes():
        for destination in get_neighbors(source):
            if show_inputs:
                label = str(get_inputs(source, destination))
                # Only draw input label if it has one
                if label == "['lambda']":
                    f.edge(source, destination)
                else:
                    f.edge(source, destination, label=label)
            else:
                f.edge(source, destination)

    # Draw graph
    f.view()


# Generate random test cases and output to file
def randomly_generate_tests(fname, num_tests, max_steps):
    """
    Write to file test cases generated by random walk

    :param fname: the output filename of generated test cases
    :param num_tests: number of generated tests
    :param max_steps: the maximum number of steps for a test
    :return: returns nothing
    """
    # Open the test file
    output = open(fname, 'w+')

    # Create the number of specified test cases
    for i in range(num_tests):
        output.write('(Start) Test Case #' + str(i+1) + '\n')
        previous_node = 'Start'

        # Create test case steps (bounded by max_steps)
        for j in range(max_steps):
            neighbors = get_neighbors(previous_node) if has_node(previous_node) else []
            if len(neighbors) > 0:
                next_node = random.choice(neighbors)
                output.write(str(j+1) + '. {' + next_node + '}')
                input_val = random.choice(get_inputs(previous_node, next_node))
                if input_val != 'lambda':
                    output.write(' [' + input_val + ']')
                previous_node = next_node
                output.write('\n')
            else:
                # End traversal if node has no neighbors
                break

        output.write('\n')


# Generate tests with a random weighted traversal and output to file
def generate_weighted_tests(fname, num_tests, min_steps, max_steps):
    """
    Write to file test cases generated by weighted probability walk

    :param fname: the output filename of generated test cases
    :param num_tests: number of generated tests
    :param min_steps: the minimum number of steps in a test
    :param max_steps: the maximum number of steps for a test
    :return: returns nothing
    """
    # Open the test file
    output = open(fname, 'w+')

    total_ending_weight = get_total_ending_weight()

    # Create the number of specified test cases
    for i in range(num_tests):
        output.write('(Start) Test Case #' + str(i+1) + '\n')
        previous_node = 'Start'
        # Create test case steps (bounded by max_steps)
        for j in range(max_steps):
            neighbors = get_neighbors(previous_node) if has_node(previous_node) else []
            if len(neighbors) > 0:
                # Get next node from neighbor probability distribution
                next_node = np.random.choice(neighbors,
                                                p=get_neighbor_probability_distribution(previous_node))

                output.write(str(j+1) + '. {' + next_node + '}')

                # Get input from input probability distribution
                input_val = np.random.choice(get_inputs(previous_node, next_node),
                                                p=get_input_probability_distribution(previous_node, next_node))

                if input_val != 'lambda':
                    output.write(' [' + input_val + ']')

                previous_node = next_node
                output.write('\n')

                # End traversal if ending weight probabilty ends test
                if j > min_steps and random.random() < \
                        (get_ending_weight(previous_node) / total_ending_weight):
                    break

            else:
                # End traversal if node has no neighbors
                break

        output.write('\n')

def generate_model_tests(fname, vec_model, reg_model, num_tests, min_steps, max_steps, xgboost=False, average=False):
    """
    Write to file test cases generated by regression model walk

    :param fname: the output filename of generated test cases
    :param vec_model: model used to embed test case into a vector
    :param reg_model: model used to calculate probabilities for walk
    :param num_tests: number of generated tests
    :param min_steps: the minimum number of steps in a test
    :param max_steps: the maximum number of steps for a test
    :return: returns nothing
    """
    # Open the test file
    output = open(fname, 'w+')

    total_ending_weight = get_total_ending_weight()

    # Create the number of specified test cases
    for i in range(num_tests):
        output.write('(Start) Test Case #' + str(i+1) + '\n')
        previous_node = 'Start'
        
        # String used for current test case, which will be evaluated by model
        current_step = ""
        previous_steps = ""

        # Create test case steps (bounded by max_steps)
        for j in range(max_steps):
            neighbors = get_neighbors(previous_node) if has_node(previous_node) else []
            if len(neighbors) > 0:
                # Store all possible next steps
                possible_next_steps = []
                # Build up probability distribution
                probabilty_distribution = []
                for neighbor in neighbors:
                    # Create a test case for each neighbor input combination
                    for input_val in get_inputs(previous_node, neighbor):
                        # Add current neighbor input combination
                        current_step = f"{j+1}. {{{neighbor}}}"
                        if input_val != "lambda":
                            current_step += f" [{input_val}]"
                        # Store as a possible choice
                        possible_next_steps.append(current_step)
                        # Vectorize test case
                        vectorized_test = d2v.get_test_case_vec(vec_model, previous_steps + current_step, average=average)
                        # Add probability of it being a bug to collection
                        if xgboost:
                            probabilty_distribution.append(reg_model.predict(xgb.DMatrix(np.array([vectorized_test])))[0][1])
                        else:
                            probabilty_distribution.append(reg_model.predict_proba([vectorized_test])[0][1])
                # Typecast probability distribution to nparray
                probabilty_distribution = np.array(probabilty_distribution)
                # Normalizing constant to make probabilites add up to 1
                k = 1 / probabilty_distribution.sum()
                # Normalize probabilities
                probabilty_distribution = np.multiply(probabilty_distribution, k)
                # Choose next step
                current_step = np.random.choice(possible_next_steps, p=probabilty_distribution)
                # Write next step to output file
                output.write(f"{current_step}\n")
                # Add the new step to the current test
                previous_steps += f"{current_step}\n"
                # Set the previous node to the selected step
                previous_node = re.search(r'(?<=\{).*(?=\})', current_step).group(0)
                
                # End traversal if ending weight probabilty ends test
                if j > min_steps and random.random() < \
                        (get_ending_weight(previous_node) / total_ending_weight):
                    break
            else:
                # End traversal if node has no neighbors
                break
        
        output.write('\n')

def output_active_learning_ranking(fname, vec_model, reg_model, xgboost=False, average=False):
    """
    Print out ranking of the generated tests for active learning

    :param fname: the filename of the generated test cases
    :param vec_model: embedding model that converts test case to vector
    :param reg_model: regression model to predict a label
    :return: returns nothing
    """
    # Read in all test cases from file
    generated_test_cases = open(fname, "r")
    t = generated_test_cases.read()

    # Split the test cases by their title
    test_cases = re.split(r'\(Start\).*?\n', t)
    test_cases.pop(0)

    X = []
    # Get vector representation of each test case and store in data
    for test_case in test_cases:
        X.append(d2v.get_test_case_vec(vec_model, test_case, average=average))
    
    entropy_vals = []
    # Calculate uncertainty for each prediction
    for i, x in enumerate(X):
        class_probs = []
        if xgboost:
            class_probs = reg_model.predict(xgb.DMatrix(np.array([x])))[0]
        else:
            class_probs = reg_model.predict_proba([x])[0]
        entropy = class_probs[0] * np.log(class_probs[0])
        entropy += class_probs[1] * np.log(class_probs[1])
        entropy *= -1
        entropy_vals.append((i+1, entropy))
    
    # Sort entropy values for uncertainty sampling
    entropy_vals.sort(key=lambda x: x[1], reverse=True)
    
    print("\n=== Highest Uncertainty ===")
    for tup in entropy_vals[:int(len(entropy_vals)*0.10)]:
        print(f"Test Case #{tup[0]}: {tup[1]}")

def evaluate_generated_tests(fname, walk_type):
    """
    Print out percentage of generated test cases that are bugs

    :param fname: the filename of the generated test cases
    :param walk_type: the type of walk that generated the test cases
    """
    # Read in all test cases from file
    generated_test_cases = open(fname, "r")
    t = generated_test_cases.read()

    # Split the test cases by their title
    test_cases = re.split(r'\(Start\).*?\n', t)
    test_cases.pop(0)

    # Counts used for final calculation
    num_tests = len(test_cases)
    bug_count = 0

    for test in test_cases:
        # Check for cheese flags bug
        bug_matches = []
        bug_matches.append('{<Logged Out> By.LINK_TEXT, "Gouda"}')
        bug_matches.append('{<Logged Out> By.LINK_TEXT, "Havarti"}')
        bug_matches.append('{<Logged Out> By.LINK_TEXT, "Camembert"}')
        bug_matches.append('{<Logged In> By.LINK_TEXT, "Paneer"}')
        bug_matches.append('{<Logged In> By.LINK_TEXT, "Havarti"}')
        bug_matches.append('{<Logged In> By.LINK_TEXT, "Gouda"}')
        if any(x in test for x in bug_matches):
            bug_count += 1
            continue
        # Check for sign up bug
        if '{<Logged Out> By.ID, "id_password2"} ["willthiswork123"]' in test and \
            '{<Logged Out> By.XPATH, "//button[text()=\"Sign Up »\"]"}' in test:
            bug_count += 1
    
    # Print out the walk performance
    print(f"\nPercentage of Bugs in {walk_type} Walk: {bug_count / num_tests}")

if __name__ == '__main__':
    # Read in user input
    parser = argparse.ArgumentParser()
    parser.add_argument("test_cases", type=str,
                        help="daniel file with input test cases to build graph")
    parser.add_argument("max_steps", type=int,
                        help="specify number of max steps for each generated test case")
    parser.add_argument("num_tests", type=int,
                        help="specify number of test cases to be generated")
    parser.add_argument("-fb", "--fixed_bugs", type=str,
                        help="daniel file with test cases of fixed bugs; edge weights * 2.0")
    parser.add_argument("-nf", "--new_features", type=str,
                        help="daniel file with test cases of new features; edge weights * 1.5")
    parser.add_argument("-ki", "--known_issues", type=str,
                        help="daniel file with test cases of known issues; edge weights * 0.5")
    parser.add_argument("-rw", "--random_walk", type=int,
                        help="specify number of test cases to be generated in random walk output")
    parser.add_argument("-mw", "--model_walk", type=int,
                        help="specify number of test cases to be generated in model walk output")
    parser.add_argument("-xgbm", "--xgboost_model_walk", type=int,
                        help="specify number of test cases to be generated in xgboost model walk output")
    parser.add_argument("-s", "--seed", type=int,
                        help="specify seed for reproducing results")
    parser.add_argument("-em", "--evaluate", action="store_true",
                        help="boolean flag to evaluate performance with test train split")
    parser.add_argument("-ae", "--average_embeddings", action="store_true",
                        help="boolean flag to average vector embeddings together")
    parser.add_argument("-el", "--edge_labels", action="store_true",
                        help="boolean flag to draw input edge labels on graph")
    parser.add_argument("-gs", "--graph_structure", action="store_true",
                        help="boolean flag to print out graph object structure")
    parser.add_argument("-ew", "--ending_weights", action="store_true",
                        help="boolean flag to print out ending weights of each node")

    args = parser.parse_args()

    if args.seed:
        random.seed(args.seed)
        np.random.seed(args.seed)

    # Build the graph from test_cases file
    build_graph(args.test_cases)

    if args.fixed_bugs:
        build_graph(args.fixed_bugs, 2)

    if args.new_features:
        build_graph(args.new_features, 1.5)

    if args.known_issues:
        build_graph(args.known_issues, 0.5)

    # Draw the graph
    draw_graph(show_inputs=args.edge_labels)

    # Generate test cases to write to file
    generate_weighted_tests("generated_test_cases.daniel", num_tests=args.num_tests,
                            min_steps=MIN_STEPS, max_steps=args.max_steps)
    if args.evaluate:
        evaluate_generated_tests("generated_test_cases.daniel", "Weighted")

    # Generate random walk test cases if specified
    if args.random_walk:
        randomly_generate_tests("random_walk_test_cases.daniel", num_tests=args.random_walk,
                                max_steps=args.max_steps)
        if args.evaluate:
            evaluate_generated_tests("random_walk_test_cases.daniel", "Random")
    
    # Variables used for the model walk
    (vec_model, X, y) = (None, None, None)
    (vec_model_avg, X_avg, y_avg) = (None, None, None)

    # Initialize variables for model walk if either kind of walk is specified
    if args.model_walk or args.xgboost_model_walk:
        (vec_model, X, y) = d2v.load_test_cases(args.test_cases)

    if args.average_embeddings:
        (vec_model_avg, X_avg, y_avg) = d2v.load_test_cases(args.test_cases, average=True)

    # Generate model walk test cases if specified
    if args.model_walk:
        reg_model = d2v.get_logistic_reg_model(X, y, evaluate=args.evaluate)
        xgb_model = d2v.get_XGBoost_model(X, y, evaluate=args.evaluate)
        if args.evaluate:
            # Read in all test cases from validation test set file
            all_test_cases = open("validation_set.daniel", "r")
            t = all_test_cases.read()

            # Get labels from file
            y_valid = []
            titles = re.findall(r'\(Start\).*?\n', t)
            for title in titles:
                # Grab the label, the last character in the title
                y_valid.append(int(title[-2]))

            # Split the test cases by their title
            test_cases = re.split(r'\(Start\).*?\n', t)
            test_cases.pop(0)

            X_valid = []
            # Get vector representation of each test case and store in data
            for test_case in test_cases:
                X_valid.append(d2v.get_test_case_vec(vec_model, test_case))
            
            # Print out validation set performance
            d2v.evaluate_validation_data(X_valid, y_valid, reg_model)
            d2v.evaluate_xgb_validation_data(X_valid, y_valid, xgb_model)
            # evaluate_generated_tests("model_test_cases.daniel", "Model") 

        generate_model_tests("model_test_cases.daniel", vec_model, reg_model, num_tests=args.model_walk,
                                    min_steps=MIN_STEPS, max_steps=args.max_steps)
        output_active_learning_ranking("model_test_cases.daniel", vec_model, reg_model)

        # Average test cases together
        if args.average_embeddings:
            reg_model_avg = d2v.get_logistic_reg_model(X_avg, y_avg, evaluate=args.evaluate)
            generate_model_tests("model_avg_test_cases.daniel", vec_model_avg, reg_model_avg, num_tests=args.model_walk,
                                        min_steps=MIN_STEPS, max_steps=args.max_steps, average=True)
            output_active_learning_ranking("model_avg_test_cases.daniel", vec_model_avg, reg_model_avg, average=True)
            if args.evaluate:
                evaluate_generated_tests("model_avg_test_cases.daniel", "Average Model")
    
    # Generate XGBoost model walk test cases if specified
    if args.xgboost_model_walk:
        reg_model = d2v.get_XGBoost_model(X, y, evaluate=args.evaluate)
        generate_model_tests("xgboost_model_test_cases.daniel", vec_model, reg_model, num_tests=args.xgboost_model_walk,
                                    min_steps=MIN_STEPS, max_steps=args.max_steps, xgboost=True)
        output_active_learning_ranking("xgboost_model_test_cases.daniel", vec_model, reg_model, xgboost=True)
        if args.evaluate:
            evaluate_generated_tests("xgboost_model_test_cases.daniel", "XGBoost Model")

        # Average test cases together
        if args.average_embeddings:
            reg_model_avg = d2v.get_XGBoost_model(X_avg, y_avg, evaluate=args.evaluate)
            generate_model_tests("xgboost_avg_model_test_cases.daniel", vec_model_avg, reg_model_avg, num_tests=args.xgboost_model_walk,
                                        min_steps=MIN_STEPS, max_steps=args.max_steps, xgboost=True, average=True)
            output_active_learning_ranking("xgboost_avg_model_test_cases.daniel", vec_model_avg, reg_model_avg, xgboost=True, average=True)
            if args.evaluate:
                evaluate_generated_tests("xgboost_avg_model_test_cases.daniel", "XGBoost Average Model")

    # Print out graph object to command line
    if args.graph_structure:
        print("\n=== Graph Object Structure ===")
        pprint.pprint(graph)

    # Print out ending weights to command line
    if args.ending_weights:
        print("\n=== Ending Weights ===")
        pprint.pprint(ending_weights)
